package top.hmtools.wxmp.core.httpclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

/**
 * 基础的httpclient
 * @author HyboWork
 *
 */
public abstract class BaseHttpclientHandle {

protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 全局通用的配置类
	 */
	protected WxmpConfiguration wxmpConfiguration;

	public WxmpConfiguration getWxmpConfiguration() {
		return wxmpConfiguration;
	}

	public void setWxmpConfiguration(WxmpConfiguration wxmpConfiguration) {
		this.wxmpConfiguration = wxmpConfiguration;
	}
}
