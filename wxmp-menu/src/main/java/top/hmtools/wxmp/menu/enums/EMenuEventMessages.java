package top.hmtools.wxmp.menu.enums;

import top.hmtools.wxmp.menu.models.eventMessage.ClickEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.LocationSelectEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.PicPhotoOrAlbumEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.PicSysPhotoEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.PicWeixinEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.ScancodePushEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.ScancodeWaitmsgEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.ViewEventMessage;
import top.hmtools.wxmp.menu.models.eventMessage.ViewMiniprogramEventMessage;

/**
 * 自定义菜单事件实体类枚举集合
 * @author HyboWork
 *
 */
public enum EMenuEventMessages {
	
	Click_Event_Message("点击菜单拉取消息时的事件推送",ClickEventMessage.class),
	View_Event_Message("点击菜单跳转链接时的事件推送",ViewEventMessage.class),
	Scancode_Push_Event_Message("scancode_push：扫码推事件的事件推送",ScancodePushEventMessage.class),
	Scancode_Waitmsg_Event_Message("scancode_waitmsg：扫码推事件且弹出“消息接收中”提示框的事件推送",ScancodeWaitmsgEventMessage.class),
	PicSys_Photo_Event_Message("pic_sysphoto：弹出系统拍照发图的事件推送",PicSysPhotoEventMessage.class),
	PicPhoto_Or_Album_Event_Message("pic_photo_or_album：弹出拍照或者相册发图的事件推送",PicPhotoOrAlbumEventMessage.class),
	Pic_Weixin_Event_Message("pic_weixin：弹出微信相册发图器的事件推送",PicWeixinEventMessage.class),
	Location_Select_Event_Message("location_select：弹出地理位置选择器的事件推送",LocationSelectEventMessage.class),
	View_Miniprogram_Event_Message("点击菜单跳转小程序的事件推送",ViewMiniprogramEventMessage.class)
	;

	private String eventName;
	
	private Class<?> className;
	
	private String remark;
	
	private EMenuEventMessages(String eName,Class<?> clazz) {
		this.eventName = eName;
		this.className = clazz;
	}

	public String getEventName() {
		return eventName;
	}

	public Class<?> getClassName() {
		return className;
	}

	public String getRemark() {
		return remark;
	}

	
	
}
