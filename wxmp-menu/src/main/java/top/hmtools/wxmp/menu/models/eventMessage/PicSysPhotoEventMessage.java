package top.hmtools.wxmp.menu.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseEventMessage;

/**
 * pic_sysphoto：弹出系统拍照发图的事件推送
 * <br>
 * <p>
 * <xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
<FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
<CreateTime>1408090651</CreateTime>
<MsgType><![CDATA[event]]></MsgType>
<Event><![CDATA[pic_sysphoto]]></Event>
<EventKey><![CDATA[6]]></EventKey>
<SendPicsInfo><Count>1</Count>
<PicList><item><PicMd5Sum><![CDATA[1b5f7c23b5bf75682a53e7b6d163e185]]></PicMd5Sum>
</item>
</PicList>
</SendPicsInfo>
</xml>
 * </p>
 * @author Hybomyth
 *
 */
public class PicSysPhotoEventMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5708780121110549653L;

	/**
	 * 发送的图片信息
	 */
	@XStreamAlias("SendPicsInfo")
	private SendPicsInfo sendPicsInfo;

	/**
	 * 发送的图片信息
	 * @return
	 */
	public SendPicsInfo getSendPicsInfo() {
		return sendPicsInfo;
	}

	/**
	 * 发送的图片信息
	 * @param sendPicsInfo
	 */
	public void setSendPicsInfo(SendPicsInfo sendPicsInfo) {
		this.sendPicsInfo = sendPicsInfo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void configXStream(XStream xStream) {
		xStream.alias("item", SendPicItem.class);
	}

	@Override
	public String toString() {
		return "PicSysPhotoEventMessage [sendPicsInfo=" + sendPicsInfo + ", event=" + event + ", eventKey=" + eventKey
				+ ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime
				+ ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}

	
}
