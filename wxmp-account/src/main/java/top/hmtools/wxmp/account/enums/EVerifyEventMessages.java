package top.hmtools.wxmp.account.enums;

import top.hmtools.wxmp.account.models.eventMessage.AnnualRenew;
import top.hmtools.wxmp.account.models.eventMessage.NamingVerifyFail;
import top.hmtools.wxmp.account.models.eventMessage.NamingVerifySuccess;
import top.hmtools.wxmp.account.models.eventMessage.QualificationVerifyFail;
import top.hmtools.wxmp.account.models.eventMessage.QualificationVerifySuccess;
import top.hmtools.wxmp.account.models.eventMessage.VerifyExpired;

/**
 * 自定义菜单事件实体类枚举集合
 * @author HyboWork
 *
 */
public enum EVerifyEventMessages {
	
	qualification_verify_success("资质认证成功（此时立即获得接口权限）",QualificationVerifySuccess.class),
	qualification_verify_fail("资质认证失败",QualificationVerifyFail.class),
	naming_verify_success("名称认证成功（即命名成功）",NamingVerifySuccess.class),
	naming_verify_fail("名称认证失败（这时虽然客户端不打勾，但仍有接口权限）",NamingVerifyFail.class),
	annual_renew("年审通知",AnnualRenew.class),
	verify_expired("认证过期失效通知",VerifyExpired.class)
	;

	private String eventName;
	
	private Class<?> className;
	
	private String remark;
	
	private EVerifyEventMessages(String eName,Class<?> clazz) {
		this.eventName = eName;
		this.className = clazz;
	}

	public String getEventName() {
		return eventName;
	}

	public Class<?> getClassName() {
		return className;
	}

	public String getRemark() {
		return remark;
	}

	
	
}
