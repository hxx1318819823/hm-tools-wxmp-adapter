package top.hmtools.wxmp.user.model.eventMessage;

import org.junit.Test;

import com.github.jsonzou.jmockdata.JMockData;

import top.hmtools.wxmp.core.model.message.BaseMessage;
import top.hmtools.wxmp.user.enums.EUserEventMessages;

public class LocationMessageTest {

	@Test
	public void testToStr(){
		EUserEventMessages[] values = EUserEventMessages.values();
		for(EUserEventMessages item:values){
			BaseMessage message = (BaseMessage)JMockData.mock(item.getClassName());
			System.out.println(message);
			System.out.println(message.toXmlMsg());
		}
	}

}
