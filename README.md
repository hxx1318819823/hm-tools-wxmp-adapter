# hm-tools-wxmp-adapter

[![996.icu](https://img.shields.io/badge/link-996.icu-red.svg)](https://996.icu)

#### 前言
同微信服务器交互，是以http方式交互，总体为主动请求微信服务器获取反馈数据、被动接收微信服务器发送的数据并处理之后反馈数据至微信服务器。即：
- 主动请求微信服务器
	- get方式
	  - 获取的数据类型为json
	  - 获取的数据类型为 字节流（比如素材管理中的获取图片素材接口）
	- post方式
	  - 请求参数数据类型为json，获取反馈的数据类型为json
	  - 请求参数数据类型为form表单，获取反馈的数据类型为json （比如素材管理中上传图片素材接口）
- 被动接收微信服务器的请求，接收到的数据类型均为xml  
  
关于微信接口收发数据类型分析可参照[微信公众号接口分析.xlsx](documents/微信公众号接口分析.xlsx)

#### 介绍
对微信公众号API接口的封装，官方API文档地址：[https://mp.weixin.qq.com/wiki](https://mp.weixin.qq.com/wiki)  
**封装微信接口过程是极度辛苦的，因为腾讯的文档总是写的不明不白，客服也总是联系不上！~**    
本工具包，参照并借鉴了mybatis、spring MVC的一些设计思路，故使用起来会有熟悉的感觉~

#### 架构说明
- `/hm-tools-wxmp-adapter/wxmp-core`		本工具包的核心，主要实现 accessToken 的获取、管理、缓存、扩展（自定义分布式方案等），http方式请求微信服务器，基础的数据类型，Javabean与xml的相互转换工具
- `/hm-tools-wxmp-adapter/wxmp-menu`		自定义菜单相关api接口、请求/响应数据结构以及事件通知数据结构封装。对应微信公众号开发文档“[自定义菜单](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1434698695)”章节。
- `/hm-tools-wxmp-adapter/wxmp-message`			消息管理相关api接口、请求/响应数据结构以及事件通知数据结构封装。对应微信公众号开发文档“[消息管理](https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Receiving_standard_messages.html)”章节。
- `/hm-tools-wxmp-adapter/wxmp-webpage`			微信网页开发相关接口，对应微信对应微信公众号开发文档“[微信网页开发](https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/iOS_WKWebview.html)”章节。
- `/hm-tools-wxmp-adapter/wxmp-material`			素材管理相关api接口、请求/响应数据结构封装。对应微信对应微信公众号开发文档“[素材管理](https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/New_temporary_materials.html)”章节
- `/hm-tools-wxmp-adapter/wxmp-comment`			图文消息管理相关api接口、请求/响应数据结构封装。对应微信对应微信公众号开发文档“[图文消息管理](https://developers.weixin.qq.com/doc/offiaccount/Comments_management/Image_Comments_Management_Interface.html)”章节
- `/hm-tools-wxmp-adapter/wxmp-user`			用户管理相关api接口、请求/响应数据结构封装。对应微信对应微信公众号开发文档“[用户管理](https://developers.weixin.qq.com/doc/offiaccount/User_Management/User_Tag_Management.html)”章节
- `/hm-tools-wxmp-adapter/wxmp-account`			账号管理相关api接口、请求/响应数据结构封装。对应微信对应微信公众号开发文档“[账号管理](https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html)”章节

#### 封装进度
[查看封装进度](接口封装进展.md)

#### 使用说明
###### 最基础的使用方式
```
//以获取自定义菜单为例
WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();//初始化配置对象实例
wxmpConfiguration.setAppid(AppId.appid);//设置APPID
wxmpConfiguration.setAppsecret(AppId.appsecret);//设置APPsecret
WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
WxmpSessionFactory factory = builder.build(wxmpConfiguration);
WxmpSession wxmpSession = factory.openSession();//获取会话
IMenuApi menuApi = this.wxmpSession.getMapper(IMenuApi.class);//获取功能接口对象实例

MenuWapperBean menu = menuApi.getMenu();
System.out.println(JSON.toJSONString(menu));
```

###### 优雅一点的使用方式

###### 整合spring boot

#### 各子组件使用说明
- [核心包](wxmp-core/readme.md)
- [自定义菜单](wxmp-menu/readme.md)
- [消息管理](wxmp-message/readme.md)
- [微信网页开发](wxmp-webpage/readme.md)
- [素材管理](wxmp-material/readme.md)
- [图文消息管理](wxmp-comment/readme.md)
- [用户管理](wxmp-user/readme.md)
- [账号管理](wxmp-account/readme.md)


#### 如何使用本组件的快照版本

1. 在自己的私服建立一个快照代理仓库即可。https://oss.sonatype.org/content/repositories/snapshots/
![如何使用快照版本](/images/howToUseSnapshot.jpg)

2. 将建好的参考纳入公共库
![如何使用快照版本](/images/howToUseSnapshot-2.jpg)
