package top.hmtools.wxmp.material.model;

public class ItemResult {

	private String media_id;
	
	/**
	 * 图文消息的具体内容，支持HTML标签，必须少于2万字符，小于1M，且此处会去除JS
	 */
	private ContentResult content;
	
	/**
	 * 文件名称
	 */
	private String name;
	
	/**
	 * 这篇图文消息素材的最后更新时间
	 */
	private long update_time;
	
	/**
	 * 图文页的URL，或者，当获取的列表是图片素材列表时，该字段是图片的URL
	 */
	private String url;

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public ContentResult getContent() {
		return content;
	}

	public void setContent(ContentResult content) {
		this.content = content;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(long update_time) {
		this.update_time = update_time;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "ItemResult [media_id=" + media_id + ", content=" + content + ", name=" + name + ", update_time="
				+ update_time + ", url=" + url + "]";
	}

	
}
