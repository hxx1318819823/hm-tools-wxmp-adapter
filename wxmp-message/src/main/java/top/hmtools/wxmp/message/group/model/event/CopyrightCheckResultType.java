package top.hmtools.wxmp.message.group.model.event;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class CopyrightCheckResultType {

	@XStreamAlias(value = "Count")
	protected String count;
	@XStreamAlias(value = "ResultList")
	protected ResultListType resultList;
	@XStreamAlias(value = "CheckState")
	protected String checkState;

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public ResultListType getResultList() {
		return resultList;
	}

	public void setResultList(ResultListType resultList) {
		this.resultList = resultList;
	}

	public String getCheckState() {
		return checkState;
	}

	public void setCheckState(String checkState) {
		this.checkState = checkState;
	}

	@Override
	public String toString() {
		return "CopyrightCheckResultType [count=" + count + ", resultList=" + resultList + ", checkState=" + checkState
				+ "]";
	}

}
