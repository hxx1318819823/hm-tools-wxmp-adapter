package top.hmtools.wxmp.message.group.model.openIdGroupSend;

/**
 * 图片：
 * @author HyboWork
 *
 */
public class ImageOpenIdGroupSendParam extends BaseOpenIdGroupSendParam {

	private MediaId image;

	public MediaId getImage() {
		return image;
	}

	public void setImage(MediaId image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "ImageOpenIdGroupSendParam [image=" + image + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}

}
