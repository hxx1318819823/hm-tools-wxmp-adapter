package top.hmtools.wxmp.message.customerService.model.sendMessage;

/**
 * Auto-generated: 2019-08-25 19:11:13
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Miniprogrampage {

	private String title;
	private String appid;
	private String pagepath;
	private String thumb_media_id;

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getAppid() {
		return appid;
	}

	public void setPagepath(String pagepath) {
		this.pagepath = pagepath;
	}

	public String getPagepath() {
		return pagepath;
	}

	public void setThumb_media_id(String thumb_media_id) {
		this.thumb_media_id = thumb_media_id;
	}

	public String getThumb_media_id() {
		return thumb_media_id;
	}

}